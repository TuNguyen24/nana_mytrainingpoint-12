var express = require('express');
var sinhvien = require('../controller/ctrol_sinhvien');

var app = express();

app.get('/',sinhvien.GetTrangChu);
app.get('/hoatdong',sinhvien.GetHoatDong);
app.get('/hoatdong/:id',sinhvien.GetHoatDongChiTiet);
app.get('/hoatdong/dangki/:id',sinhvien.GetDangKiHD);
app.get('/hoatdong/huydangki/:id',sinhvien.GetHuyDangKi)
app.get('/thamgia',sinhvien.GetThamGia);
app.get('/thamgia/:id',sinhvien.GetDaThamGia);
app.get('/thamgia/huythamgia/:id',sinhvien.GetBoThamGia);
app.get('/goiy',sinhvien.GetGoiY);
app.post('/goiy/goiydiem',sinhvien.PostGoiYDiem);
app.get('/goiy/goiydiem/dangky/:id',sinhvien.GetDangKiHDGoiY);
app.get('/goiy/goiydiem/huydangky/:id',sinhvien.GetHuyDangKiHDGoiY)
app.get('/lotrinh',sinhvien.GetLoTrinh);
app.get('/lotrinh/xoa/:id',sinhvien.GetXoaHD);
app.get('/lienhe',sinhvien.GetLienHe);
app.get('/help',sinhvien.GetHelp);
app.get('/profile',sinhvien.GetProfile);
app.get('/doimatkhau',sinhvien.GetDoiMatKhau);
module.exports = app;